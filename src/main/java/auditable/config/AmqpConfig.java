package auditable.config;

import lombok.Data;
import org.springframework.amqp.rabbit.connection.ConnectionNameStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Configuration
@Validated
@Data
public class AmqpConfig {

  @NotNull
  @Value("${server.servlet.application-display-name}")
  private String applicationName;

  @Bean
  public ConnectionNameStrategy connectionNameStrategy() {
    return connectionFactory -> applicationName.toUpperCase() + '-' + connectionFactory.getClass().getSimpleName() + "#" + connectionFactory.hashCode();
  }
}
