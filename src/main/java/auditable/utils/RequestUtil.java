package auditable.utils;

import auditable.model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;
import java.util.StringTokenizer;
import static auditable.utils.ConstantString.BASIC;
import static auditable.utils.ConstantString.BASIC_AUTH_HEADER;
import static auditable.utils.ConstantString.FORWARDED_HEADER;
import static auditable.utils.ConstantString.ORG_CODE_HEADER;
import static auditable.utils.ConstantString.ORG_NAME_HEADER;
import static auditable.utils.ConstantString.ROLES_HEADER;

public class RequestUtil {

  private RequestUtil() {
    throw new AssertionError("No RequestUtil instances for you!");
  }

  public static HttpServletRequest getCurrentRequest() {
    final RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
    Assert.state(attrs instanceof ServletRequestAttributes, "No current ServletRequestAttributes");
    if (!(attrs instanceof ServletRequestAttributes))
      return null;
    return ((ServletRequestAttributes) attrs).getRequest();
  }

  public static String getCurrentRequestUri() {
    return ServletUriComponentsBuilder.fromCurrentRequest().toUriString();
  }

  public static String getBasicAuthHeader() {
    final HttpServletRequest httpRequest = getCurrentRequest();
    if (httpRequest == null)
      return null;
    return httpRequest.getHeader(BASIC_AUTH_HEADER);
  }

  public static String getApiRoles() {
    return getHeaderValue(ROLES_HEADER);
  }

  public static String getOrgCode() {
    return getHeaderValue(ORG_CODE_HEADER);
  }

  public static String getOrgName() {
    return getHeaderValue(ORG_NAME_HEADER);
  }

  public static String getHeaderValue(String orgCodeHeader) {
    String result = null;
    final HttpServletRequest httpRequest = getCurrentRequest();
    if (httpRequest == null)
      return null;
    final String headerValue = httpRequest.getHeader(orgCodeHeader);
    if (headerValue != null)
      result = new String(Base64Utils.decodeFromString(headerValue), StandardCharsets.UTF_8);
    return result;
  }

  public static Optional<User> getFromBasicAuth() {
    final String authorization = getBasicAuthHeader();
    if (authorization != null && authorization.startsWith(BASIC)) {
      final String base64Credentials = authorization.substring(BASIC.length()).trim();
      final String credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8"));
      final String[] values = credentials.split(":", 2);
      return Optional.of(new User(values[0], new BCryptPasswordEncoder().encode(values[1])));
    }
    return Optional.empty();
  }

  public static String getClientIpAddress() {
    final HttpServletRequest request = getCurrentRequest();
    if (request == null)
      return null;
    final String xForwardedForHeader = request.getHeader(FORWARDED_HEADER);
    if (xForwardedForHeader == null) {
      return request.getRemoteAddr();
    } else {
      return Optional
          .ofNullable(new StringTokenizer(xForwardedForHeader, ",").nextToken())
          .orElse(request.getRemoteAddr())
          .trim();
    }
  }

  public static String getClientHostName() {
    final HttpServletRequest request = getCurrentRequest();
    if (request == null)
      return null;
    return request.getRemoteHost();
  }
}