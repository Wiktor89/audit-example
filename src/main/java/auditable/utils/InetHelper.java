package auditable.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetHelper {

  public static String getIpAddressOfMachine() {
    InetAddress inetAddress;
    try {
      inetAddress = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      return null;
    }
    return inetAddress.getHostAddress();
  }

  public static String getHostAddressOfMachine() {
    InetAddress inetAddress;
    try {
      inetAddress = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      return null;
    }
    return inetAddress.getHostName();
  }

}
