package auditable.utils;

public abstract class ConstantString {

  public final static String SERVICE = "SERVICE";
  public final static String SYSTEM_REQUEST = "System request";
  public final static String BASIC = "Basic";
  public final static String FORWARDED_HEADER = "X-Forwarded-For";
  public final static String BASIC_AUTH_HEADER = "Authorization";
  public final static String ROLES_HEADER = "X-Api-Roles";
  public final static String ORG_CODE_HEADER = "X-Org-Code";
  public final static String ORG_NAME_HEADER = "X-Org-Name";
  public final static String AUTH = "/getAuth";
  public final static String API = "/api";
}
