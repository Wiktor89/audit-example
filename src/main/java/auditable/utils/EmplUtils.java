package auditable.utils;

import auditable.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Optional;

import static auditable.utils.ConstantString.BASIC;

public class EmplUtils {

  public static String getCurrentUser() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      try {
        Optional<User> username = RequestUtil.getFromBasicAuth();
        if (username.isPresent())
          return username.get().getUserName();
        else
          return Thread.currentThread().getName();
      } catch (IllegalStateException e){
        if (auditable.model.security.SecurityContextHolder.getLoggedAuthorization() != null)
          return getUsernameFromBasic(auditable.model.security.SecurityContextHolder.getLoggedAuthorization());
      }
    }
    return Optional.ofNullable(authentication.getName()).orElse(Thread.currentThread().getName());
  }

  private static String getUsernameFromBasic(String authorization) {
    try {
      String base64Credentials = authorization.substring(BASIC.length()).trim();
      String credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8"));
      // credentials = username:password
      final String[] values = credentials.split(":", 2);
      return values[0];
    } catch (IndexOutOfBoundsException e) {
      return null;
    }
  }
}
