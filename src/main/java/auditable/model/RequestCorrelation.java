package auditable.model;

public class RequestCorrelation {

  private static final ThreadLocal<String> id = new ThreadLocal<>();

  public static String getId() {
    return id.get();
  }
}