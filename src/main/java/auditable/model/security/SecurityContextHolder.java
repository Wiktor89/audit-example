package auditable.model.security;

public class SecurityContextHolder {

  public static final String AUTHORIZATION_HEADER = "Authorization";

  private static final ThreadLocal<String> threadLocalAuthorizationString = new ThreadLocal<String>();

  public final static String getLoggedAuthorization() {
    return threadLocalAuthorizationString.get();
  }

  public final static void setLoggedAuthorization(String authorization) {
    threadLocalAuthorizationString.set(authorization);
  }
}
