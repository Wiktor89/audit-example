package auditable.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Auditable {

  String eventName() default "";

  String rabbitExchangeName() default "audit.exchange";

  AuditableType auditableType() default AuditableType.RESTCONTROLLER;

  UserType userType() default UserType.CONSUMER;
}
