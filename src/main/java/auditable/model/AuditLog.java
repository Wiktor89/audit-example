package auditable.model;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
public class AuditLog {

  private Long id;

  private LocalDateTime stamp = LocalDateTime.now();

  private LocalDate logDate = stamp.toLocalDate();

  private LocalTime logTime = stamp.toLocalTime();

  private String username;

  private String eventName;

  private boolean isSuccess;

  private String ipAddress;

  private String machineName;

  private String methodName;

  private String methodArgs;

  private String result;

  private Long elapsedMillis;

  private String auditableType;

  private String correlationId;

  private Long stepNumber;
}
