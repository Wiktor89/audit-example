package auditable.model;

/**
 * Тип пользователя.
 * SERVICE - программный вызов по расписанию (@Scheduled)
 * CONSUMER - реальный пользователь
 *  Дает возможность аудировать вызовы внутри app.
 */
public enum UserType {

  SERVICE, CONSUMER
}
