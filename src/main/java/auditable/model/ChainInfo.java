package auditable.model;

import java.util.concurrent.atomic.AtomicLong;

public class ChainInfo {

  private final String identity;

  private final AtomicLong atomicLong;

  public ChainInfo() {
    this.identity = RequestCorrelation.getId();
    this.atomicLong = new AtomicLong(0);
  }

  public String getIdentity() {
    return identity;
  }

  public AtomicLong getAtomicLong() {
    return atomicLong;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("ChainInfo{");
    sb.append("identity='").append(identity).append('\'');
    sb.append(", atomicLong=").append(atomicLong);
    sb.append('}');
    return sb.toString();
  }
}
