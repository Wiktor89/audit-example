package auditable.service;

import auditable.components.MessageParser;
import auditable.model.AuditLog;
import auditable.model.Auditable;
import auditable.model.ChainInfo;
import auditable.utils.EmplUtils;
import auditable.utils.InetHelper;
import auditable.utils.RequestUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static auditable.utils.ConstantString.SERVICE;
import static auditable.utils.ConstantString.SYSTEM_REQUEST;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuditServiceImpl implements AuditService {

  private final MessageParser messageParser;
  
  private final RabbitTemplate rabbitTemplate;

  @Override
  public void audit(boolean isSuccess, Auditable auditable, String methodName, Object[] methodArgs, Object result, long elapsedMillis, ChainInfo chainInfo) {
    log.debug("Performing audit {}", auditable);
    final AuditLog logEntity = new AuditLog();
    logEntity.setSuccess(isSuccess);
    logEntity.setEventName(auditable.eventName());
    logEntity.setUsername(auditable.userType().name().equals(SERVICE) ? SYSTEM_REQUEST : EmplUtils.getCurrentUser());
    logEntity.setMethodName(methodName);
    try {
      logEntity.setIpAddress(RequestUtil.getClientIpAddress());
      logEntity.setMachineName(RequestUtil.getClientHostName());
    } catch (IllegalStateException e) {
      logEntity.setIpAddress(InetHelper.getIpAddressOfMachine());
      logEntity.setMachineName(InetHelper.getHostAddressOfMachine());
    }
    if (methodArgs != null)
      logEntity.setMethodArgs(Arrays.toString(methodArgs));
    if (result != null)
      logEntity.setResult(result.toString());
    logEntity.setElapsedMillis(elapsedMillis);
    logEntity.setAuditableType(auditable.auditableType().name());
    logEntity.setCorrelationId(chainInfo.getIdentity());
    logEntity.setStepNumber(chainInfo.getAtomicLong().incrementAndGet());
    try {
      log.trace("Created auditLog message {}", logEntity);
      final Message replyMessage = messageParser.createMessage(logEntity);
      rabbitTemplate.send(auditable.rabbitExchangeName(), "", replyMessage);
      log.trace("send AuditLog message exchange {} data={}", auditable.rabbitExchangeName(), replyMessage);
    } catch (JsonProcessingException ex) {
      log.error("Error in auditLog " + auditable, ex);
    }
  }
}

