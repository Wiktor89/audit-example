package auditable.service;

import auditable.model.Auditable;
import auditable.model.ChainInfo;

public interface AuditService {

  void audit(boolean isSuccess, Auditable auditable, String methodName, Object[] methodArgs, Object result, long elapsedMillis, ChainInfo chainInfo);
}
