package auditable.service;

import auditable.model.Auditable;
import auditable.model.ChainInfo;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class AuditAdvice {

  private final ThreadLocal<ChainInfo> correlation = new ThreadLocal<>();

  private final AuditService auditService;

  @Autowired
  public AuditAdvice(AuditService auditService) {
    this.auditService = auditService;
  }

  @Around("@annotation(auditable)")
  public Object audit(final ProceedingJoinPoint point, final Auditable auditable) throws Throwable {
    boolean ok = false;
    boolean isFirstInChain = false;
    if (correlation.get() == null) {
      final ChainInfo chainInfo = new ChainInfo();
      isFirstInChain = true;
      correlation.set(chainInfo);
    }
    Object o = null;
    final long startTime = System.currentTimeMillis();
    try {
      o = point.proceed();
      ok = true;
      return o;
    } catch (Exception ex) {
      o = ex;
      throw ex;
    } finally {
      try {
        final long elapsedTime = System.currentTimeMillis() - startTime;
        auditService.audit(ok, auditable, point.getSignature().getName(), point.getArgs(), o, elapsedTime, correlation.get());
      } catch (Exception ex) {
        log.error("Error in auditService.audit " + auditable, ex);
      } finally {
        if (isFirstInChain)
          correlation.set(null);
      }
    }
  }
}
