package auditable.components;

import auditable.config.AmqpConfig;
import auditable.exception.ParseMessageException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

@Component
@Slf4j
public class MessageParserImpl implements MessageParser {

  private final AmqpConfig amqpConfig;

  private final ObjectMapper mapper;

  @Autowired
  public MessageParserImpl(AmqpConfig amqpConfig, ObjectMapper mapper) {
    this.amqpConfig = amqpConfig;
    this.mapper = mapper;
    this.mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
  }

  @Override
  public <T> T tryParseMessage(String message, Class<T> valueType) {
    log.trace("tryParseMessage[{}] to Class[{}]", message, valueType.getSimpleName());
    final T response;
    try {
      response = mapper.readValue(message, valueType);
    } catch (IOException e) {
      final String s = String.format("Error in parsing [%s] to Class %s exception %s", message, valueType.getSimpleName(), e.getMessage());
      log.error(s, e);
      throw new ParseMessageException(s, e, message, valueType);
    }
    log.trace("Parsed MessageToClass {}", response);
    return response;
  }

  @Override
  public String writeValueAsString(Object value) throws JsonProcessingException {
    log.trace("writeValueAsString from Class[{}] value=[{}]", value.getClass().getSimpleName(), value);
    try {
      return mapper.writeValueAsString(value);
    } catch (JsonProcessingException e) {
      log.error("Error in writeValueAsString", e);
      throw e;
    }
  }

  @Override
  public Message createMessage(Object value) throws JsonProcessingException {
    log.trace("createMessage from Class[{}] value=[{}]", value.getClass().getSimpleName(), value);
    try {
      final byte[] body = mapper.writeValueAsBytes(value);
      Message message = MessageBuilder.withBody(body)
          .setContentType(MessageProperties.CONTENT_TYPE_JSON)
          .setContentEncoding(String.valueOf(StandardCharsets.UTF_8))
          .setContentLength((long) body.length)
          .setAppId(amqpConfig.getApplicationName())
          .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
          .setTimestamp(new Date())
          .build();
      log.trace(message.toString());
      return message;
    } catch (Exception ex) {
      log.error("Error in createMessage", ex);
      throw ex;
    }
  }
}
